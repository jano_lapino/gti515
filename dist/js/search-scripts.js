/**
 * Created by yan on 2016-11-22.
 */
var data = [];
var clickedItemId;

window.addEventListener('DOMContentLoaded', function() {

  // Execute a reservation when user clicks the button
  document.getElementById("reserve").onclick = function() {reserveBook()};
  $('#reserverdError').hide();
  $('#reservedSuccess').hide();

});

$(function () {
  $("#grid").shieldGrid({
    dataSource: {
      data: gridData,
      filter: {
        and: [
          { path: "name", filter: "contains", value: "b" },
          {
            or: [
              { path: "gender", filter: "eq", value: "male" },
              { path: "name", filter: "startswith", value: "c" }
            ]
          }
        ]
      }
    },
    scrolling:true,
    sorting:{
      multiple: false,
      allowUnsort: false
    },
    selection: {
      type: "row",
      multiple: false
    },
    events: {
      selectionChanged: onSelectionChanged
    },
    paging: true,
    columns: [
      { field: "id", width: "70px", title: "ISBN" },
      { field: "name", title: "Titre du livre", width: "170px" },
      { field: "owner", title: "Propriétaire" },
      { field: "phone", title: "Numéro de téléphone", width: "170px", sortable:false },
      { field: "price", title: "Prix" }
    ]
  });
  var dataSource = $("#grid").swidget().dataSource,
    input = $("#filterbox input"),
    timeout,
    value;
  input.on("keydown", function () {
    clearTimeout(timeout);
    timeout = setTimeout(function () {
      value = input.val();
      if (value) {
        dataSource.filter = {
          or: [
            { path: "id", filter: "contains", value: value },
            { path: "name", filter: "contains", value: value },
            { path: "owner", filter: "contains", value: value },
            { path: "phone", filter: "contains", value: value },
            { path: "price", filter: "contains", value: value }
          ]
        };
      }
      else {
        dataSource.filter = null;
      }
      dataSource.read();
    }, 300);
  });
});

function onSelectionChanged(e) {

  var itemClickData = $("#grid").swidget().contentTable.find(".sui-selected").get(0);

  var id = itemClickData.cells[0].innerHTML;
  var title = itemClickData.cells[1].innerHTML;
  var owner = itemClickData.cells[2].innerHTML;
  var phone = itemClickData.cells[3].innerHTML;
  var price = itemClickData.cells[4].innerHTML;

  clickedItemId = id;

  $('.isbn').html(id);
  $('.title').html(title);
  $('.owner').html(owner);
  $('.phone').html(phone);
  $('.price').html(price);


  if(localStorage.data != null){
    var bookArray = $.parseJSON(localStorage.data);
    var result = $.grep(bookArray, function(e){ return e.id == clickedItemId; });
    if (result.length == 0) {
      $('#myModal').modal('show');
      $('#reserverdError').hide();
    }else{
      $('#reserverdError').show();
      $('#reservedSuccess').hide();
    }
  }else{
    $('#myModal').modal('show');
    $('#reserverdError').hide();
  }


}

function reserveBook() {
  data.push({
    id: clickedItemId
  });
  $('#reservedSuccess').show();
  localStorage.data = JSON.stringify(data);

  updateCart();
}
