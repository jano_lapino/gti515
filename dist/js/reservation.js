/**
 * Created by Yan on 2016-11-23.
 */
window.addEventListener('DOMContentLoaded', function() {
  $('#deleteSuccess').hide();
});

$(function () {
  $("#grid").shieldGrid({
    dataSource: {
      data: gridData,
      filter: {
        and: [
          { path: "id", filter: "contains", value: "" },
        ]
      }
    },
    scrolling:true,
    sorting:{
      multiple: false,
      allowUnsort: false
    },
    selection: {
      type: "row",
      multiple: false
    },
    paging: true,
    columns: [
      { field: "id", width: "70px", title: "ISBN" },
      { field: "name", title: "Titre du livre", width: "100px" },
      { field: "owner", title: "Propriétaire", width: "100px"},
      { field: "phone", title: "Numéro de téléphone", width: "170px", sortable:false },
      { field: "price", title: "Prix", width:"50px" },
      {
        title: "Réservation",
        width: "100px",
        buttons: [
          { cls: "deleteButton", commandName: "delete", caption: "<img src='' /><span onclick=''>Enlever</span>" }
        ]
      }
    ],
    events:
    {
      command: function (e) {
        if(e.commandName == "delete"){
          var itemClickData = $("#grid").swidget().contentTable.find(".sui-selected").get(0);
          var id = itemClickData.cells[0].innerHTML;
          /*var result = $.grep(localStorage.data, function(e){
            return e.id == id;
          });*/
          var bookArray = $.parseJSON(localStorage.data);
          $.each(bookArray, function(i){
            if(bookArray[i].id === id) {
              bookArray.splice(i,1);
              localStorage.data = JSON.stringify(bookArray);
              $('#deleteSuccess').show();
              return false;
            }
          });
          updateCart();
        }
      },
      filterWidgetCreating: function (e) {
        if (true) {
          e.options = { max: 2 };
        }
      }
    }
  });


  var dataSource = $("#grid").swidget().dataSource,
    input = $("#filterbox input"),
    timeout,
    value;
  input.on("keydown", function () {
    clearTimeout(timeout);
    timeout = setTimeout(function () {
      value = input.val();
      if (value) {
        dataSource.filter = {
          or: [
            { path: "id", filter: "contains", value: value },
            { path: "name", filter: "contains", value: value },
            { path: "owner", filter: "contains", value: value },
            { path: "phone", filter: "contains", value: value },
            { path: "price", filter: "contains", value: value }
          ]
        };
      }
      else {
        dataSource.filter = null;
      }
      dataSource.read();
    }, 300);
  });

  var bookIDs = JSON.parse(localStorage.data);
  var grid = {
    or: []
  };

  //Construct the filter for the reserved
  for(var j in bookIDs) {

    var item = bookIDs[j];

    grid.or.push({
      "path"    : "id",
      "filter"  : "eq",
      "value"   : item.id
    });
  }

  //If the grid is empty
  if(grid.or.length === 0){
    grid.or.push({
      "path"    : "id",
      "filter"  : "eq",
      "value"   : -1
    });
  }

  $("#grid").swidget().filter(grid);

});

function deleteAction(e) {
  alert("del");
}
